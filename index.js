import express from 'express';
import logger from 'morgan';
import bodyParser from 'body-parser';
import open from 'open';
import http from 'http';

import './dotenv';

const app = express();
app.use(logger('dev'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('*', (req, res) => {
  return res.json({
    message: 'Welcome to the start',
  });
});

const port = parseInt(process.env.PORT, 10) || 8000;
app.set('port', port);

const server = http.createServer(app);
server.listen(port, (error) => {
  if (error) {
    winston.log(error);
  } else {
    open(`http://localhost:${port}`);
  }
});
