
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('BlockItems', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      content: {
        type: Sequelize.STRING
      },
      complete: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      blockId: {
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'Blocks',
          key: 'id',
          as: 'blockId',
        },
      },
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('BlockItems');
  }
};
