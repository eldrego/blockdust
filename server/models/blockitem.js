
module.exports = (sequelize, DataTypes) => {
  const BlockItem = sequelize.define('BlockItem', {
    content: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    complete: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    }
  }, {
    classMethods: {
      associate: (models) => {
        BlockItem.belongsTO(models.Block, {
          foreignKey: 'blockId',
          onDelete: 'CASCADE',
        });
      }
    }
  });
  return BlockItem;
};
