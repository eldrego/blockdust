module.exports = (sequelize, DataTypes) => {
  const Block = sequelize.define('Blocks', {
    title: {
      type: DataTypes.STRING,
      allownNull: false,
    }
  }, {
    classMethods: {
      associate: (models) => {
        Block.hasMany(models.BlockItem, {
          foreignKey: 'blockId',
          as: 'BlockItems',
        });
      },
    },
  });
  return Block;
};
